# pylint: disable=line-too-long
# pylint: disable=invalid-name

from typing import Tuple
from typing import List
from typing import Union
import array

import math


class BaseArray:
    """
    Razred BaseArray, za učinkovito hranjenje in dostopanje do večdimenzionalnih podatkov. Vsi podatki so istega
    številskega tipa: int ali float.

    Pri ustvarjanju podamo obliko-dimenzije tabele. Podamo lahko tudi tip hranjenih podatkov (privzeto je int) in
    podatke za inicializacijo (privzeto vse 0).

    Primeri ustvarjanja:
    a = BaseArray((10,)) # tabela 10 vrednosti, tipa int, vse 0
    a = BaseArray((3, 3), dtype=float) # tabela 9 vrednost v 3x3 matriki, tipa float, vse 0.0
    a = BaseArray((2, 3), dtype=float, dtype=float, data=(1., 2., 1., 2., 1., 2.)) # tabela 6 vrednost v 2x3 matriki,
                                                                               #tipa float, vrednosti 1. in 2.
    a = BaseArray((2, 3, 4, 5), data=tuple(range(120))) # tabela dimenzij 2x3x4x5, tipa int, vrednosti od 0 do 120


    Do podatkov dostopamo z operatorjem []. Pri tem moramo paziti, da uporabimo indekse, ki so znotraj oblike tabele.
    Začnemo z indeksom 0, zadnji indeks pa je dolžina dimenzije -1.
     Primeri dostopanja :

    a = BaseArray((2, 3, 4, 5))
    a[0, 1, 2, 1] = 10 # nastavimo vrednost 10 na mesto 0, 1, 2, 1
    v = a[0, 0, 0, 0] # preberemo vrednost na mestu 0, 0, 0 ,0

    a[0, 3, 0, 0] # neveljaven indeks, ker je vrednost 3 enaka ali večja od dolžine dimenzije

    Velikost tabele lahko preberemo z lastnostjo 'shape', podatkovni tip pa z 'dtype'. Primer:

    a.shape # to vrne vrednost  (2, 3, 4, 5)
    a.dtype # vrne tip int

    Čez tabelo lahko iteriramo s for zanko, lahko uporabimo operatorje/ukaze 'reversed' in 'in'.

    a = BaseArray((4,2), data=(1, 2, 3, 4, 5, 6, 7, 8))
    for v in a: # for zanka izpiše vrednost od 1 do 8
        print(v)
    for v in reversed(a): # for zanka izpiše vrednost od 8 do 1
        print(v)

    2 in a # vrne True
    -1 in a # vrne False
    """
    def __init__(self, shape: Tuple[int], dtype: type = None, data: Union[Tuple, List] = None):
        """
        Create an initialize a multidimensional myarray of a selected data type.
        Init the myarray to 0.

        :param shape: tuple or list of integers, shape of constructed myarray
        :param dtype: type of data stored in myarray, float or int
        :param data: list of tuple of data values, must contain consistent types and
                     have a length that matches the shape.
        """

        _check_shape(shape)
        self.__shape = (*shape,)

        self.__steps = _shape_to_steps(shape)

        if dtype is None:
            dtype = float
        if dtype not in [int, float]:
            raise Exception('Type must by int or float.')
        self.__dtype = dtype

        n_elements = 1
        for s in self.__shape:
            n_elements *= s

        if data is None:
            if dtype == int:
                self.__data = array.array('i', [0]*n_elements)
            elif dtype == float:
                self.__data = array.array('f', [0]*n_elements)
        else:
            if len(data) != n_elements:
                raise Exception(f'shape {shape:} does not match provided data length {len(data):}')

            for d in data:
                if not isinstance(d, (int, float)):
                    raise Exception(f'provided data does not have a consistent type')
            self.__data = [d for d in data]

    @property
    def shape(self):
        """
        :return: a tuple representing the shape of the myarray.
        """
        return self.__shape

    @property
    def dtype(self):
        """
        :return: the type stored in the myarray.
        """
        return self.__dtype

    def __test_indice_in_range(self, ind):
        """
        Test if the given indice is in within the range of this myarray.
        :param ind: the indices used
        :return: True if indices are valid, False otherwise.
        """
        for ax in range(len(self.__shape)):
            if ind[ax] < 1 or ind[ax] > self.shape[ax]:
                raise Exception('indice {:}, axis {:d} out of bounds [1, {:}]'.format(ind, ax, self.__shape[ax]))

    def __getitem__(self, indice):
        """
        Return a value from this myarray.
        :param indice: indice to this myarray, integer or tuple
        :return: value at indice
        """
        if not isinstance(indice, tuple):
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        return self.__data[key_lin]

    def __setitem__(self, indice, value):
        """
        Set a value in this myarray.
        :param indice: valued indice to a position, integer of tuple
        :param value: value to set
        :return: does not return
        """
        if not isinstance(indice, tuple):
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        self.__data[key_lin] = value

    def __iter__(self):
        for v in self.__data:
            yield v

    def __reversed__(self):
        for v in reversed(self.__data):
            yield v

    def __contains__(self, item):
        return item in self.__data

    def _print_matrix(self):
        if(len(self.shape)==1):
            for x in self.__data:
                print(x, end=" ")
            print()
        elif(len(self.shape)==2):
            for x in range(self.__shape[0]):
                for y in range((self.shape[1])):
                    print(self.__data[y+( self.__shape[1]*x)], end =" ")
                print()
        elif(len(self.shape)==3):
            for x in range(self.__shape[0]):
                for y in range((self.shape[1])):
                    for z in range((self.shape[2])):
                         print(self.__data[(x*self.__shape[1]*self.__shape[2])+(y*self.__shape[2])+z], end =" ")
                    print("    ", end = "")
                print()
    def v2Darray(self):
        maboy =  [[0 for x in range(self.shape[1])] for y in range(self.shape[0])]
        for y in range(self.shape[0]):
            for x in range(self.shape[1]):
                maboy[y][x] = self.__data[x+y*self.shape[1]]
        return maboy

    def sort_matrix(self, vrstice):

        if (len(self.shape) == 1):
            mergeSort(self.__data)
        elif (len(self.shape) == 2):
            if vrstice == True:
                temp = []
                arr = self.v2Darray()
                arr = self.transponiraj(arr)
                for y in range(len(arr)):
                    for x in range(len(arr[0])):
                        temp.append(arr[y][x])
                self.__data = temp
                a = [self.shape[1],self.shape[0]]



            if vrstice == False:
                a = self.shape
            for x in range(self.__shape[0]):
                temp = self.__data[x*a[1]:(x+1)*a[1]]
                mergeSort( temp)
                self.__data[x * a[1]:(x + 1) *a[1]] = temp
                print()
            if vrstice:
                out = [None] * len(self.__data)
                for y in range(self.shape[1]):
                    for x in range(self.shape[0]):
                        out[self.shape[1]*x+y] = self.__data[x+y*self.shape[0]]

                self.__data=out




        elif (len(self.shape) == 3):

            for x in range(self.__shape[0]):
                for y in range((self.shape[1])):
                    print ("od " + str((x+1)*(y+1)*self.shape[2]+x*self.shape[2]*self.shape[1]-self.shape[2]) + " do "+ str((x+1) * (y+1) * self.shape[2] + x *self.shape[2] * self.shape[1] ))
                    temp = self.__data[(x+1)*(y+1)*self.shape[2]+x*self.shape[2]*self.shape[1]-self.shape[2]:(x+1) * (y+1) * self.shape[2] + x *self.shape[2] * self.shape[1]]
                    mergeSort(temp)
                    self.__data[(x+1)*(y+1)*self.shape[2]+x*self.shape[2]*self.shape[1]-self.shape[2]:(x+1) * (y+1) * self.shape[2] + x *self.shape[2] * self.shape[1]] = temp;
                print()

    def setType(self):
        for x in self.__data:
            if(type(x) != self.dtype):
                self.__dtype = float
                return




    def transponiraj(self, maboy):
        out = list()

        maboy = [list(i) for i in zip(*maboy)]
        return maboy


    def _find_in_matrix(self, findMe):
        if(len(self.shape)==1):
            for x in range(len(self.__data)):
                if(self.__data[x] == findMe):
                 print('('+ str(x)+')')

        elif(len(self.shape)==2):
            for x in range(self.__shape[0]):
                for y in range((self.shape[1])):
                    if (self.__data[y+( self.__shape[1]*x)] == findMe):
                        print('('+str(x)+','+str(y)+')')

        elif(len(self.shape)==3):
            for x in range(self.__shape[0]):
                for y in range((self.shape[1])):
                    for z in range((self.shape[2])):
                         if findMe == (self.__data[(x*self.__shape[1]*self.__shape[2])+(y*self.__shape[2])+z]) :
                             print('('+str(x)+','+str(y)+','+str(z)+')')
        print()

    def __add__(self, other):
      if type(other) is BaseArray:
        try:
            for i in range(len(self.shape)):
                if(self.shape[i] != other.shape[i]):
                    print("napacne velikosti")
                    return
            for x in range(len(self.__data)):
                self.__data[x]+=other.__data[x]
            self.setType()
            return self
               # print(self.__data[x])

        except:
            print("Addition error")
            return self
      else:
          try:
              for i in range(len(self.__data)):
                    self.__data[i]+=other
              self.setType()
              return self

          except:
              print("Addition error")
              return self
    def __sub__(self, other):
      if type(other) is BaseArray:
        try:
            for i in range(len(self.shape)):
                if(self.shape[i] != other.shape[i]):
                    print("napacne velikosti")
                    return
            for x in range(len(self.__data)):
                self.__data[x]-=other.__data[x]
                self.setType()
            return self
               # print(self.__data[x])

        except:
            print("sub error")
            return self
      else:
          try:
              for i in range(len(self.__data)):

                    self.__data[i]-=other
              self.setType()
              return self

          except:
              print("sub error")
              return self
    def __mul__(self, other):
     if type(other) is BaseArray:
      if len(other.shape)==2 and len(self.shape)==2:
       if other.shape[0] ==  self.shape[1]:

         try:

            out =[0 for x in range(self.shape[0]*other.shape[1])]
            X = self.v2Darray()
            Y = other.v2Darray()
            result = [[0 for x in range(self.shape[0])] for y in range(other.shape[1])]
            for i in range(len(X)):
                # iterate through columns of Y
                for j in range(len(Y[0])):
                    # iterate through rows of Y
                    for k in range(len(Y)):
                        result[i][j] += X[i][k] * Y[k][j]

            for y in range(len(result)):
                for x in range(len(result[0])):
                    out[y*len(result[0])+x]=result[y][x]
            o = BaseArray((len(result),len(result[0])), dtype=float, data=out)
            self.setType()
            return o

         except :
             print("multyplication error")
         return self
      else:
          try:
              for i in range(len(self.__data)):
                    self.__data[i]*=other.__data[i]
              return self

          except:
              print("multyplication error")
              return self
     else:
      try:
        for i in range(len(self.__data)):
            self.__data[i] *= other
        return self
      except:
          print("multyplication error")
          return self


    def __truediv__(self, other):
      if type(other) is BaseArray:
        try:
            for i in range(len(self.shape)):
                if(self.shape[i] != other.shape[i]):
                    print("napacne velikosti")
                    return
            for x in range(len(self.__data)):
                self.__data[x]/=other.__data[x]
                self.setType()
            return self
               # print(self.__data[x])

        except:
            print("Addition error")
            return self
      else:
          try:
              for i in range(len(self.__data)):
                    self.__data[i]/=other
              self.setType()
              return self

          except:
              print("Addition error")
              return self
    def  exp(self, other):
        temp = self
        for x in range(other-1):
            temp *= temp
        return temp

    def log(self, base):
        temp = self
        for x in range(len(temp.__data)):
            temp.__data[x] = math.log(temp.__data[x], base)
            temp.setType()
        return temp







def _check_shape(shape):
    if not isinstance(shape, (tuple, list)):
        raise Exception(f'shape {shape:} is not a tuple or list')
    for dim in shape:
        if not isinstance(dim, int):
            raise Exception(f'shape {shape:} contains a non integer {dim:}')


def _multi_ind_iterator(multi_inds, shape):
    inds = multi_inds[0]
    s = shape[0]

    if isinstance(inds, slice):
        start, stop, step = inds.start, inds.stop, inds.step
        if start is None:
            start = 1
        if step is None:
            step = 1
        if stop is not None:
            stop += 1
        inds_itt = range(0, s+1)[start:stop:step]
    else:  # type(inds) is int:
        inds_itt = (inds,)

    for ind in inds_itt:
        if len(shape) > 1:
            for ind_tail in _multi_ind_iterator(multi_inds[1:], shape[1:]):
                yield (ind,)+ind_tail
        else:
            yield (ind,)


def _shape_to_steps(shape):
    dim_steps_rev = []
    s = 1
    for d in reversed(shape):
        dim_steps_rev.append(s)
        s *= d
    steps = tuple(reversed(dim_steps_rev))
    return steps


def _multi_to_lin_ind(inds, steps):
    i = 0
    for n, s in enumerate(steps):
        i += (inds[n]-1)*s
    return i


def _is_valid_indice(indice):
    if isinstance(indice, tuple):
        if not indice:
            return False
        for i in indice:
            if not isinstance(i, int):
                return False
    elif not isinstance(indice, int):
        return False
    return True

def mergeSort(alist):
    if len(alist)>1:
        mid = len(alist)//2
        lefthalf = alist[:mid]
        righthalf = alist[mid:]

        mergeSort(lefthalf)
        mergeSort(righthalf)

        i=0
        j=0
        k=0
        while i < len(lefthalf) and j < len(righthalf):
            if lefthalf[i] < righthalf[j]:
                alist[k]=lefthalf[i]
                i=i+1
            else:
                alist[k]=righthalf[j]
                j=j+1
            k=k+1

        while i < len(lefthalf):
            alist[k]=lefthalf[i]
            i=i+1
            k=k+1

        while j < len(righthalf):
            alist[k]=righthalf[j]
            j=j+1
            k=k+1
